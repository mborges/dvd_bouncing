from st3m.application import Application, ApplicationContext
from random import random, randrange
from math import pi, sin, cos, sqrt, atan2
from collections import namedtuple

img_margin = 5
img_width = 100-img_margin*2
img_half_width = img_width/2
img_height = 51-img_margin*2
img_half_height = img_height/2

screen_radius = 120

Point = namedtuple('Point', ['x', 'y'])

def dist(x, y):
    return sqrt(pow(x, 2) + pow(y, 2))

def angle(x, y):
    return atan2(y, x)

# Return the position of the corner pixel of the image outside of the screen if any, or None
# This is used to check if we need to bounce the logo
def outside(x, y):
    min_x = x - img_half_width
    min_y = y - img_half_height
    max_x = x + img_half_width
    max_y = y + img_half_height
    if dist(min_x, min_y) > screen_radius:
        return Point(min_x, min_y)
    if dist(min_x, max_y) > screen_radius:
        return Point(min_x, max_y)
    if dist(max_x, min_y) > screen_radius:
        return Point(max_x, min_y)
    if dist(max_x, max_y) > screen_radius:
        return Point(max_x, max_y)
    return None


class Dvd(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        while True:
            self.x = randrange(-screen_radius, screen_radius)
            self.y = randrange(-screen_radius, screen_radius)
            if not outside(self.x, self.y):
                break
        direction_angle = random() * 2 * pi
        self.direction_x = cos(direction_angle)
        self.direction_y = sin(direction_angle)

    def draw(self, ctx: Context) -> None:
        self.x += self.direction_x
        self.y += self.direction_y

        # Check for colision
        outside_pos = outside(self.x, self.y)
        if outside_pos:
            collision_x = outside_pos.x
            collision_y = outside_pos.y
            collision_dist = sqrt(collision_x * collision_x + collision_y * collision_y)
            normal_x = -collision_x / collision_dist
            normal_y = -collision_y / collision_dist

            dot2 = -2 * (normal_x * self.direction_x + normal_y * self.direction_y)
            self.direction_x = dot2 * normal_x + self.direction_x
            self.direction_y = dot2 * normal_y + self.direction_y

            # If the new position is still outside the screen, move the logo closer to the center
            while dist(self.x + self.direction_x, self.y + self.direction_y) > screen_radius:
                self.x *= 0.99
                self.y *= 0.99
            self.x += self.direction_x
            self.y += self.direction_y

        ctx.rgb(0, 0, 0).rectangle(-screen_radius, -screen_radius, screen_radius * 2, screen_radius * 2).fill()
        ctx.image(
            f"{self.app_ctx.bundle_path}/dvd.png",
            self.x - img_half_width - img_margin,
            self.y - img_half_height - img_margin,
            img_width + img_margin * 2,
            img_height + img_margin * 2,
        )

    def think(self, ins: InputState, delta_ms: int) -> None:
        pass


if __name__ == '__main__':
    from st3m.run import run_view
    run_view(Dvd(ApplicationContext()))
